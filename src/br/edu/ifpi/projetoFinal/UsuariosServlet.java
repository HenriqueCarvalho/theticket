package br.edu.ifpi.projetoFinal;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class UsuariosServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		JDOUtils jdo = new JDOUtils();
		Gson gson = new Gson();
		resp.setContentType("text/plain");
		
		List<Usuario> users = jdo.findAll(Usuario.class);
		
		if (users != null && !users.isEmpty()) {
			resp.getWriter().print(gson.toJson(users));
		} else {
			resp.getWriter().println(gson.toJson("Nao existe nenhum usuario cadastrado!"));
		}
		
	}
}
