package br.edu.ifpi.projetoFinal;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;

public class ProdutoServlet extends HttpServlet {

	private static final long serialVersionUID = 7067597087979603334L;

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		JDOUtils jdo = new JDOUtils();
		String dados = req.getParameter("data"); 
		Gson gson = new Gson();
		Produto produto = gson.fromJson(dados, Produto.class);
		List<Produto> produtos = jdo.findByAttribute(Produto.class, "nome", produto.getNome());
	
		
		if(produtos == null || produtos.isEmpty()){
			
			jdo.save(produto);
			resp.getWriter().print(gson.toJson(produto));
			
		}else{
			
			resp.getWriter().println(gson.toJson("Produto ja cadastrado!"));
			
		}
	}

	
}
