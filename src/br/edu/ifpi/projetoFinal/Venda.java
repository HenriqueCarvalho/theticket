package br.edu.ifpi.projetoFinal;

import java.math.BigDecimal;

import java.util.ArrayList;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Key;

@PersistenceCapable(detachable = "true")
public class Venda {
	
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Key id;
	@Persistent
	int qtde;
	@Persistent
	Usuario usuario;
	@Persistent(mappedBy = "produto", dependent = "true")
	ArrayList<Produto> produtos = new ArrayList<Produto>();
	
	public Key getId() {
		return id;
	}

	public void setId(Key id) {
		this.id = id;
	}

	public int getQtde() {
		return qtde;
	}

	public void setQtde(int qtde) {
		this.qtde = qtde;
	}


	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}



	public ArrayList<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(ArrayList<Produto> produtos) {
		this.produtos = produtos;
	}
	
	public BigDecimal ValorTotal(){
		System.out.println("aki entrou111111111");
		BigDecimal valorTotaldaVenda = BigDecimal.ZERO;
		for(int i=0;i<this.produtos.size();i++){
			
			BigDecimal valor = produtos.get(i).getQtdeEstoque().multiply(produtos.get(i).getValor());
			valorTotaldaVenda = valorTotaldaVenda.add(valor);
		}
		
		return valorTotaldaVenda;
		
	}
}