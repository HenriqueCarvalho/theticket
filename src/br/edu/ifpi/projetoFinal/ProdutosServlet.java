package br.edu.ifpi.projetoFinal;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class ProdutosServlet extends HttpServlet{

	private static final long serialVersionUID = -4309340815376871158L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		JDOUtils jdo = new JDOUtils();
		Gson gson = new Gson();

		resp.setContentType("text/plain");
		List<Produto> products = jdo.findAll(Produto.class);
		if (products != null && !products.isEmpty()) {
			resp.getWriter().print(gson.toJson(products));
		} else {
			
			resp.getWriter().println(gson.toJson("Nao existe nenhum produto cadastrado!"));
		}
	}

}
