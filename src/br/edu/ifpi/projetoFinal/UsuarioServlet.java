package br.edu.ifpi.projetoFinal;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@SuppressWarnings("serial")
public class UsuarioServlet extends HttpServlet {
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		JDOUtils jdo = new JDOUtils();
		String usuarioData = req.getParameter("data");
		Gson g = new Gson();
		Usuario u = g.fromJson(usuarioData, Usuario.class);
		resp.setContentType("text/plain");
		
		if(u == null || u.getEmail() == null || u.getNome() == null || u.getSenha() == null){
			
			resp.getWriter().println(g.toJson("Erro: Nao foi possivel realizar o cadastro!"));
			
		}else{
			
			List<Usuario> usuarios = jdo.findByAttribute(Usuario.class, "email", u.getEmail());
			if (usuarios == null || usuarios.isEmpty()) {
				
				jdo.save(u);
				resp.getWriter().println(g.toJson(u));
				
			} else {
				resp.getWriter().println(g.toJson("Email ja cadastrado!"));
			}
		}
		
	}
	@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			
			JDOUtils jdo = new JDOUtils();
			String usuarioData = req.getParameter("data");
			Gson g = new Gson();
			Usuario u = g.fromJson(usuarioData, Usuario.class);
			resp.setContentType("text/plain");
			
			Usuario usuarioLogado = login(u, jdo);
			if (usuarioLogado != null) {
				resp.getWriter().println(g.toJson(usuarioLogado));
				//resp.getWriter().println("login efetuado com sucesso!");
			} else {
				JsonObject obj = new JsonObject();
		        obj.erro = "Acesso Negado! Confira seu email ou senha!";
		        resp.getWriter().println(g.toJson(obj));
		        //resp.getWriter().println("{ erro : Acesso Negado!Email ou sua senha nao conferem! }");
		        //resp.getWriter().println(g.toJson("{ 'erro' : 'Acesso Negado!Email ou sua senha nao conferem!' }"));

				//resp.getWriter().println(g.toJson("Acesso Negado!Email ou sua senha nao conferem!"));
				
			}
		
		}
	
	private Usuario login(Usuario u, JDOUtils jdo) {
		List<Usuario> usuarios = jdo.findByAttribute(Usuario.class, "email", u.getEmail());
		if (usuarios != null && !usuarios.isEmpty()) {
			if (usuarios.get(0).getSenha().equals(u.getSenha())) {
				return usuarios.get(0);
			}
		}
		return null;
	}
	
	
	
	
	
	
	@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			
			//atualizar usuario
			JDOUtils jdo = new JDOUtils();
			String usuarioData = req.getParameter("data");
			Gson g = new Gson();
			Usuario u = g.fromJson(usuarioData, Usuario.class);
			resp.setContentType("text/plain");
			Usuario usuarioLogado = login(u, jdo);
			if (usuarioLogado != null) {
				if (u.getSenha() != null) {
					jdo.save(usuarioLogado);
				}
			} else {
				resp.getWriter().println(g.toJson("Atualizacao nao pode ser realizada!"));
			}
		}
	
	
}
