package br.edu.ifpi.projetoFinal;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;


public class VendaServlet extends HttpServlet {


	private static final long serialVersionUID = -8234919692903543734L;

	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		JDOUtils jdo = new JDOUtils();
		String dados = req.getParameter("data"); 
			
		Gson gson = new Gson();
		Venda v = gson.fromJson(dados,Venda.class);
		System.out.println("venda v..:"+v);
		Produto p = gson.fromJson(dados,Produto.class);
		List<Produto> carrinho = jdo.findByAttribute(Produto.class, "nome", p.getNome());
		System.out.println("carrinho c"+carrinho);
			
		if(carrinho.isEmpty()){
				
			resp.getWriter().println(("O carrinho esta vazio!Adicione um produto!"));
				
		}else{
			
			jdo.save(v);
			resp.getWriter().print(gson.toJson(v));
			resp.getWriter().println("Venda realizada com sucesso!");
			
				
		}
		
		/*List<Produto> carrinho =  jdo.findAll(Produto.class);
		System.out.println("carrinho:"+carrinho);
		
		
		if(c.isEmpty()){
			
			
			
			
			
		}else{
			
			resp.getWriter().println(gson.toJson("Venda nao pode ser realizada!"));
		}*/
	
	
	}
}
