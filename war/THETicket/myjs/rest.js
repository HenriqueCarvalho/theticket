//Funções JSON pra compras
getJSON = function( file ) {
	var request = new XMLHttpRequest();
	request.open("GET", file, false);
	request.send(null);
	return  request.responseText;
}

getJSONParsed = function (file) {
	return JSON.parse( getJSON( file ) );
}
//fim funções JSON pra compras 

function httpPut(theUrl, data) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("PUT", theUrl, false);
	var params = "data=" + JSON.stringify(data);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.send(params);
	return xmlHttp.responseText;
}

function httpPost(theUrl, data) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", theUrl, false);
	var params = "data=" + JSON.stringify(data);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.send(params);
	return xmlHttp.responseText;
}

function httpGet(theUrl, data) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", theUrl + "?data=" + JSON.stringify(data), false);
	xmlHttp.send(null);
	return xmlHttp.responseText;
}