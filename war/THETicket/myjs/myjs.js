var produto;
var cart = new Array();

$(document).ready( function() {

  //CARREGAMENTO DOS PRODUTOS
      produto = JSON.parse( httpGet('/produtos') );

      var conteudo = new EJS({ url:"THETicket/template/produtos.ejs"}).render( produto );

      $("#vendas").html(conteudo);


      //setando a quantidade de produtos no carrinho do HTML
      function strQtdCart(cart){
      if(cart.length == '0'){
        var str = 'Vazio no momento !';
      }else if(cart.length == '1'){ 
        var str = cart.length + ' ' + 'item';
      }else{
        var str = cart.length + ' ' + 'itens';
      }
      document.getElementById('qtd-cart').innerHTML = str;
      }

      function SomaProdutos(cart){
        var valor = 0.0;
        for( var i = 0; i < cart.length ; i++ ) {
          valor += parseFloat(cart[i].valor);
        }
        document.getElementById('button-pay').innerHTML = 'Comprar R$' + valor;
      }

      //CARRINHO
      $('.sm-add-button').click(function(){
        var dataAdd = $(this).attr('pos');

        if ( dataAdd ){
          cart.push( produto[ dataAdd ] );
          var conteudo = new EJS({ url: "THETicket/template/cart.ejs"}).render( cart );
          $("#sidebar-myIngress").html(conteudo);
        }

        strQtdCart(cart);        
        SomaProdutos(cart);

      });


      $('body').on('click', '.cart-remove-element', function() {
        var dataRem = $(this).attr('remove');
        if ( dataRem ){
           cart.splice( dataRem, 1 );
           var conteudo = new EJS({ url: "THETicket/template/cart.ejs"}).render( cart );
          $("#sidebar-myIngress").html(conteudo);
        }

        strQtdCart(cart);
        SomaProdutos(cart);

      });
      //FIM CARRINHO
  //FIM CARREGAMENTO DOS PRODUTOS

  //SISTEMA DE LOGIN
      //funcao pra converter o seralize em JSON
      function formToJson(form){
        formData = form.serializeArray();
        objForm = _.object(_.pluck(formData, 'name'), _.pluck(formData, 'value'));
        return JSON.parse(JSON.stringify(objForm)); 
      }

      function dataUserInterface(div, json){
        $(div).load("THEticket/dataUserInterface.html", function() { 
          document.getElementById('load-user-nome').innerHTML = json.nome;
          document.getElementById('load-user-email').innerHTML = json.email;
        });
      }

      $('.button-login').click(function(){
        url = '/usuario';
        div = '#form-login';

        json = formToJson($(div));

        obj = httpGet(url, json);

        obj = JSON.parse(obj);

        if( isNaN(obj.id) == true && isNaN(obj.erro) == true){
          $('#email-login').css('border','2px solid red');
          $('#senha-login').css('border','2px solid red');
          console.log(obj.erro);
        }else{
          document.getElementById('menu-toggle-login').innerHTML = obj.nome;
          $(div).empty();
          dataUserInterface(div, obj);
        }

      });
  //FIM SISTEMA DE LOGIN

  //SISTEMA DE CADASTRO

  		function emailValidate(email){
  			data = httpGet('usuarios');
  			obj  = JSON.parse(data);

  			for (var i = 0; i < obj.length; i++) {
  				if(obj[i].email == email){
  					return false;
  				} 
  			};

  			return true;
  		}

  		/*$('#email-create').focus(function(){
  			var url = '/usuario';
  			var str = "";

  			$("#email-create").keyup(function(){


  				disabled = emailValidate($('#email-create').val());

  				console.log(disabled);

				if(disabled == true){
					document.getElementById('alert-email').innerHTML = "email disponivel!";
					$('#alert-email').css('color','green');
					$('.button-create').removeAttr('disabled');
				}else{
					document.getElementById('alert-email').innerHTML = "email já cadastrado!";
					$('#alert-email').css('color','red');
					$('.button-create').attr('disabled','disabled');
				}

			});


  		});*/


		$('.button-create').click(function(){
			url = '/usuario';
			div = '#form-create';

			json = formToJson($(div));

			console.log(json);

			if(json.nome != "" && json.email != "" && json.senha != ""){

				if(emailValidate($('#email-create').val()) == true){
					console.log(httpPut(url, json));
				}else{
					$('#alert-email').css('color','red');
				}

			}else{
				console.log("não salva nada!");
			}

		});
  //FIM SISTEMA DE CADASTRO

	//PAGAMENTO
		$('#button-pay').click(function(){

			if($('#qtd-cart').text() == "Vazio no momento !"){

				msg = "Seu carrinho está vazio no momento!";

			}else{

				if( $('#menu-toggle-login').text() == "Login" ){

					msg = "Caro usuario você precisa se logar para finalizar a compra!"

				}else{

					msg = "Obrigado pela compra em instantes seu ingresso estará chegando no seu e-mail!";
					
				}

			}			

			document.getElementById('mensage-pay').innerHTML = msg;
		});
	//FIM PAGAMENTO


      //side-bar cart
      $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#sidebar-wrapper").toggleClass("active");
          $(".js-grayout").css('display','block');
      });

      $("#menu-close-cart").click(function(e) {
          e.preventDefault();
          $("#sidebar-wrapper").toggleClass("active");
          $(".js-grayout").css('display','none');
      });

     
      //side-bar login
      $("#menu-toggle-login").click(function(e) {
          e.preventDefault();
          $("#sidebar-login").toggleClass("active");

          if( $(".js-grayout").is(":visible") ){
            $(".js-grayout").css('display','none');
          }else{
            $(".js-grayout").css('display','block');
          }

      });

      $("#menu-close-login").click(function(e) {
          e.preventDefault();
          $("#sidebar-login").toggleClass("active");
          $(".js-grayout").css('display','none');
      });

      //side-bar create
      $("#menu-toggle-create").click(function(e) {
          e.preventDefault();
          $("#sidebar-create").toggleClass("active");
      });

      $("#menu-close-create").click(function(e) {
          e.preventDefault();
          $("#sidebar-create").toggleClass("active");
      });

     $(".js-button-pay").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
        $(".js-grayout").css('display','none');
     });

      $("[rel='tooltip']").tooltip();    
 
      $('.js-thumbnail').hover(
          function(){
              $(this).find('.caption').fadeIn(500);
          },
          function(){
              $(this).find('.caption').fadeOut(300);
          }
      ); 

      //função para rolar a pagina automaticamente
      $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
      });

      // INICIO EFEITO TRANSFER
      // run the currently selected effect
      function runEffect(e) {
        var options = { to: ".button_02", className: "ui-effects-transfer" };
        // run the effect
        $(e).effect( "transfer", options, 1000, callback );
      };
      // callback function to bring a hidden box back
      function callback() {
        setTimeout(function() {
          $( "#effect" ).removeAttr( "style" );
        }, 1000 );
      };
      // set effect from select menu value
      $( ".button_01" ).click(function() {
        runEffect($(this).parent());
        return false;
      });
      // FIM EFEITO TRANSFER


});