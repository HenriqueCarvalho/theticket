
/**
 * strCookie = Nome do cookie
 * strValor = Valor que será salvo no cookie
 * lngDias = Dias de validade do cookie
 */
function createCookie(strCookie, strValor, lngDias) {
  $.cookie(strCookie, strValor, {
    expires : lngDias
  });
}


/**
 * nomeCookie = Nome que foi dado ao cookie durante a criação
 */
function readCookie(nomeCookie) {
  if ( $.cookie(nomeCookie) == 1 )
    return true;
  else
  	return false;
}


 /**
 * strCookie = Nome do cookie
 */
function deleteCookie(strCookie) {
  $.cookie(strCookie, null);
}